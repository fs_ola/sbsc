import 'package:flutter/material.dart';
import 'package:sbsc/News.dart';

class NewsDetails extends StatelessWidget {
  NewsDetails(this.news);
  final News news;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('News Details'),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 5),
              child: Image.network(news.imageUrl),
            ),
            Text(
              news.title,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Text(news.description)
            )
          ],
        ),
      ),
    );
  }
}