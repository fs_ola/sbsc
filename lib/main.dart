import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:sbsc/News.dart';
import 'package:sbsc/constants.dart';
import 'package:sbsc/news_details.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'News',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'NewsAPI Demo App'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<List<News>> newsList;

  @override
  void initState() {
    super.initState();
    newsList = getNews();
  }

  Future<List<News>> getNews() async {
    String url = '${ENDPOINT}everything?q=apple&apiKey=$API_KEY';
    print('URL: $url');
    http.Response response = await http.get(url);

    if (response.statusCode == 200) {
      var newsJson = json.decode(response.body);
      var newsArticles = newsJson['articles'];
      List<News> result = [];
      for (var i = 0; i < newsArticles.length; i++) {
        result.add(News.fromMap(newsArticles[i]));
      }
      print(result.first.title);
      return result;
    } else {
      throw Exception('Failed');
    }
  }

  @override
  Widget build(BuildContext context) {
    getNews();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Container(
          child: FutureBuilder<List<News>>(
            future: newsList,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                print(snapshot.data.first.imageUrl);
                // return Text('${snapshot.data.length}');
                return ListView.builder(
                  itemBuilder: (BuildContext context, int index) {
                    final News news = snapshot.data[index];
                    return Card(
                      elevation: 8.0,
                      child: ListTile(
                        leading: Image.network(news.imageUrl),
                        title: Text(
                          news.title
                        ),
                        subtitle: Padding(
                          padding: EdgeInsets.all(5),
                          child: Text(
                            news.description
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                            context, 
                            MaterialPageRoute(
                              builder: (BuildContext context) => NewsDetails(news)
                            )
                          );
                        },
                      )
                    );
                  },
                  itemCount: snapshot.data.length,
                );
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }

              return Text('Loading News');
            },
          )
        )
      )
    );
  }
}
