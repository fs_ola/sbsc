class News {
  String _id;
  String title;
  String description;
  String imageUrl;

  News({
    String id,
    this.title: '',
    this.description: '',
    this.imageUrl: ''
  }): this._id = id;

  factory News.fromMap(Map<String, Object> data) {
    return News(
      title: data['title'],
      description: data['description'],
      imageUrl: data['urlToImage']
    );
  }

  String get id => this._id;
  set id(String id) => this._id = id;
}